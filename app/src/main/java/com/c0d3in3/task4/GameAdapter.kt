package com.c0d3in3.task4

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.cube_item_layout.view.*

class GameAdapter(private val items: ArrayList<CubeModel>, private val callback : CustomCallback) : RecyclerView.Adapter<GameAdapter.ViewHolder>() {

    interface CustomCallback{
        fun setItemType(position: Int)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        fun onBind(){
            val model = items[adapterPosition]
            when (model.type) {
                "X" -> itemView.cubeImageButton.setBackgroundResource(R.mipmap.baseline_close_black_24)
                "O" -> itemView.cubeImageButton.setImageResource(R.mipmap.baseline_exposure_zero_black_24)
                else -> itemView.cubeImageButton.setImageResource(0)
            }
            itemView.cubeImageButton.setOnClickListener {
                callback.setItemType(adapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.cube_item_layout, parent, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }
}