package com.c0d3in3.task4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), GameAdapter.CustomCallback {

    //player false = X
    //player true = O
    private val cubes = arrayListOf<CubeModel>()
    private val adapter = GameAdapter(cubes, this)
    private var player = false
    private var cubesLeft = 0
    private var winner = false
    private var gameSize = 0
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init(){
        okButton.setOnClickListener {
            val number = numberEditText.text.toString().toInt()
            if(number in 3..5){
                cubes.clear()
                gameRecyclerView.layoutManager = GridLayoutManager(this, number)
                gameRecyclerView.adapter = adapter
                cubesLeft = number*number
                gameSize = number
                winner = false
                player = false
                (0 until number*number).forEach {
                    cubes.add(CubeModel(it, ""))
                }
                adapter.notifyDataSetChanged()
            }
        }
    }

    private fun checkWinner(){
        if(cubesLeft == 0 && !winner){
            Toast.makeText(this, "Tie", Toast.LENGTH_SHORT).show()
            winner = true
        }
        if(!winner)checkHorizontal()
        if(!winner)checkVertical()
        if(!winner)checkFirstDiagonals()
        if(!winner) checkSecondDiagonal()
    }

    private fun checkFirstDiagonals() {
        var checker = 0
        val first = cubes[0].type
        for (idx in 0 until gameSize * gameSize step gameSize + 1) {
            if (cubes[idx].type == first && cubes[idx].type != "") checker++
            if (checker == gameSize) {
                Toast.makeText(this, "Winner player $first", Toast.LENGTH_SHORT).show()
                winner = true
                break
            }
        }
    }

    private fun checkSecondDiagonal() {
        var checker = 0
        val first = cubes[gameSize - 1].type
        for (idx in gameSize - 1 until (gameSize * gameSize) - (gameSize - 1) step gameSize - 1) {
            if (cubes[idx].type == first && cubes[idx].type != "") checker++
            if (checker == gameSize) {
                Toast.makeText(this, "Winner player $first", Toast.LENGTH_SHORT).show()
                winner = true
                break
            }
        }
    }

    private fun checkHorizontal(){
        for(idx in 1..gameSize){
            var checker = 0
            for(cube in gameSize*(idx-1) until (gameSize*idx)) {
                val first = cubes[gameSize*(idx-1)].type
                if(cubes[cube].type == first && cubes[cube].type != "") checker++
                if(checker == gameSize){
                    Toast.makeText(this, "Winner player $first", Toast.LENGTH_SHORT).show()
                    winner = true
                    break
                }
            }
        }
    }

    private fun checkVertical(){
        for(idx in 0..gameSize){
            var checker = 0
            for(cube in idx until gameSize*gameSize step gameSize) {
                val first = cubes[idx].type
                if(cubes[cube].type == first && cubes[cube].type != "") checker++
                if(checker == gameSize){
                    Toast.makeText(this, "Winner player $first", Toast.LENGTH_SHORT).show()
                    winner = true
                    break
                }
            }
        }
    }

    override fun setItemType(position: Int) {
        if(winner) return Toast.makeText(this, "Game is over, please start again", Toast.LENGTH_SHORT).show()
        if(cubes[position].type == ""){
            if(player){
                cubes[position].type = "O"
                player = false
            }
            else{
                cubes[position].type = "X"
                player = true
            }
            cubesLeft--
            checkWinner()
            adapter.notifyItemChanged(position)
        }
        else{
            Toast.makeText(this, "You cant do this!", Toast.LENGTH_SHORT).show()
        }
    }
}
